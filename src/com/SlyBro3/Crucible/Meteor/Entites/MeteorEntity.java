package com.SlyBro3.Crucible.Meteor.Entites;

import net.minecraft.server.v1_7_R4.EntityLargeFireball;
import net.minecraft.server.v1_7_R4.MovingObjectPosition;
import net.minecraft.server.v1_7_R4.World;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.LargeFireball;
import org.bukkit.entity.Player;

import com.SlyBro3.Crucible.Meteor.Main;
import com.SlyBro3.Crucible.Meteor.Math.GenRandom;

@SuppressWarnings("unused")
public class MeteorEntity extends EntityLargeFireball {
    public MeteorEntity(World world) {
        super(world);
    }

    private float speed = 1.05F;
    private float trail = 3F;
    private float impact = 8F;
    public static boolean firey;

    @Override
    public void h() {
        LargeFireball f = (LargeFireball) this.getBukkitEntity();
        Chunk c = f.getLocation().getChunk();
        m();
        super.h();
    }

    public void m() {
        LargeFireball f = (LargeFireball) this.getBukkitEntity();
        
        int doExplode = 0;
        if (doExplode == 5000 || doExplode >= 5000) {
            doExplode = 0;
        }
        if (doExplode == 0 || doExplode <= 5000) {
            if (GenRandom.gen(100, 0) < 78) {
                f.getWorld().createExplosion(f.getLocation(), trail);
            }
            doExplode += 1;
        } else {
            doExplode -= 1;
        }
        this.motX *= speed;
        this.motY *= speed;
        this.motZ *= speed;
    }

    @Override
    public void a(MovingObjectPosition movingobjectposition) {
        LargeFireball f = (LargeFireball) this.getBukkitEntity();
        contactExplode(f);
        broadcast(f);
        changeBlock(f, 10, 33);
        die();
    }
    int broadcast;
    int msgBroadcast = 0;
    private Main plugin = Main.plugin;
    public void broadcast(LargeFireball f) {
        final int coordX = (int) Math.ceil(f.getLocation().getX()
                - GenRandom.genNeg(46, -68));
        final int coordZ = (int) Math.ceil(f.getLocation().getZ()
                - GenRandom.genNeg(46, -68));
        final int coordY = (int) Math.ceil(f.getLocation().getY()
                + GenRandom.gen(9, 0));
        broadcast = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
            @SuppressWarnings("deprecation")
			public void run() {
                if (msgBroadcast == 0) {
                    Bukkit.broadcastMessage("�4�lA METEOR HAS LANDED!");
                    for(Player p : Bukkit.getOnlinePlayers()){
                    	p.playSound(p.getLocation(), Sound.PISTON_RETRACT, 1, 1);
                    }
                    msgBroadcast += 1;
                } else if (msgBroadcast == 1) {
                    Bukkit.broadcastMessage("�7 - Locate the Meteorite.");
                    for(Player p : Bukkit.getOnlinePlayers()){
                    	p.playSound(p.getLocation(), Sound.PISTON_RETRACT, 1, 1);
                    }
                    msgBroadcast += 1;
                } else if (msgBroadcast == 2) {
                    Bukkit.broadcastMessage("�7 - Secure and Mine the ore as quickly as possible.");
                    for(Player p : Bukkit.getOnlinePlayers()){
                    	p.playSound(p.getLocation(), Sound.PISTON_RETRACT, 1, 1);
                    }
                    msgBroadcast += 2;
                } else if (msgBroadcast == 3) {

                    msgBroadcast += 1;
                } else if (msgBroadcast == 4) {
                    Bukkit.broadcastMessage("�eOur some-what working satellites predict the crash site somewhat near there: �b�lX: "
                            + coordX + " Y: " + coordY + " Z: " + coordZ + "�r");
                    for(Player p : Bukkit.getOnlinePlayers()){
                    	p.playSound(p.getLocation(), Sound.CHEST_CLOSE, 1, 1);
                    }
                    msgBroadcast += 1;
                } else if (msgBroadcast == 5) {
                    Bukkit.broadcastMessage("�7 - Give or take 50 blocks on X and Z.");
                    for(Player p : Bukkit.getOnlinePlayers()){
                    	p.playSound(p.getLocation(), Sound.CHEST_CLOSE, 1, 1);
                    }
                    msgBroadcast += 1;
                } else if (msgBroadcast == 6) {
                    Bukkit.broadcastMessage("�7 - �lGet in, get out, get �a�lpaid�7�l.");
                    for(Player p : Bukkit.getOnlinePlayers()){
                    	p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
                    }
                    msgBroadcast += 1;
                }else{}
                if(msgBroadcast == 7 || msgBroadcast >= 7){
                    Bukkit.getServer().getScheduler().cancelTask(broadcast);;
                }
            }
        }, 20, 40);
    }

    public void changeBlock(LargeFireball f, int radius, int chance) {
        int r = radius;
        int baseX = f.getLocation().getBlockX();
        int baseY = f.getLocation().getBlockY();
        int baseZ = f.getLocation().getBlockZ();
        
        for(int x = baseX - r; x <= baseX + r; x++){
            for(int y = baseY - r; y <= baseY + r; y++){
                for(int z = baseZ - r; z <= baseZ + r; z++){
                    double d = ((baseX - x) * (baseX - x) + ((baseZ - z) * (baseZ - z)) + ((baseY - y) * (baseY - y)));
                    if(d<r*r&&(d<((r-1)*(r-1)))){
                        Location l = new Location(f.getWorld(), x, y, z);
                        Block b = f.getLocation().getWorld().getBlockAt(l );
                        int rand = GenRandom.gen(100, 0);
                        if (rand <= chance) {
                            if (b.getType() == Material.BEDROCK
                                    || b.getType() == Material.AIR
                                    || b.getType() == Material.WATER
                                    || b.getType() == Material.STATIONARY_LAVA
                                    || b.getType() == Material.STATIONARY_WATER
                                    || b.getType() == Material.LAVA
                                    || b.getType() == Material.QUARTZ_ORE) {
                            } else {
                                int blk = GenRandom.gen(100, 0);
                                if (blk <= 90) {
                                    b.setType(Material.QUARTZ_ORE);
                                } else {
                                    b.setType(Material.LAVA);
                                }

                            }
                        } else {
                        }
                    }
                    }
                }
            }
    }
    public void contactExplode(LargeFireball f) {
        f.getWorld().createExplosion(f.getLocation(), impact, firey);
    }
    public void loadChunk(LargeFireball f){
        //Get all other 8 Chunks surrounding the center chunk, also loads Center Chunk. Center Chunk (c) also called Current Chunk
        Chunk c = f.getLocation().getChunk();
        Location midLeft = f.getLocation().add(-16 , 0 , 0);
        Location midRight = f.getLocation().add(+16 , 0 , 0);
        
        Location upperCenter = f.getLocation().add(0 , 0 , +16);
        Location upperLeft = f.getLocation().add(+16 , 0 , +16);
        Location upperRight = f.getLocation().add(-16 , 0 , +16);
        
        Location lowRight = f.getLocation().add(-16, 0, -16);
        Location lowCenter = f.getLocation().add(0 , 0 , -16);
        Location lowLeft = f.getLocation().add(+16 , 0 , -16);
        
        //Loading all the chunks, if you do getLocation#getChunk it also loads it, no need for c#load.
        
        Chunk midLeftChunk = midLeft.getChunk();
        Chunk midRightChunk = midRight.getChunk();
        
        Chunk upperCenterChunk = upperCenter.getChunk();
        Chunk upperLeftChunk = upperLeft.getChunk();
        Chunk upperRightChunk = upperRight.getChunk();
        
        Chunk lowRightChunk = lowRight.getChunk();
        Chunk lowCenterChunk = lowCenter.getChunk();
        Chunk lowLeftChunk = lowLeft.getChunk();
        
    }
}
