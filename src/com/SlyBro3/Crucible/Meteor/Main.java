package com.SlyBro3.Crucible.Meteor;

import org.bukkit.plugin.java.JavaPlugin;

import com.SlyBro3.Crucible.Meteor.Commands.CommandMeteor;

public class Main extends JavaPlugin {
    public static Main plugin;

    public void onEnable() {
    	plugin = this;
        this.getCommand("meteor").setExecutor(new CommandMeteor(this));
        System.out
                .println("[Meteor] version 1 was sucessfully enabled!");
    }

    public void onDisable() {
    	plugin = null;
        System.out
                .println("[Meteor] version 1 was sucessfully disabled!");
    }
}
