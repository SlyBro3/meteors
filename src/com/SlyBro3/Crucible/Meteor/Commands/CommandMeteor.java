package com.SlyBro3.Crucible.Meteor.Commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_7_R4.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

import com.SlyBro3.Crucible.Meteor.Main;
import com.SlyBro3.Crucible.Meteor.Entites.MeteorEntity;
import com.SlyBro3.Crucible.Meteor.Math.GenRandom;

public class CommandMeteor implements CommandExecutor {

    public Main plugin;

    public CommandMeteor(Main main) {
        plugin = main;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label,
            String[] args) {
        if (label.equalsIgnoreCase("meteor")) {
            if (sender instanceof Player) {
            	if(sender.getName().contains("SlyBro3") || sender.getName().contains("xshmeven") || sender.getName().contains("Creepz21")){
                Player p = (Player) sender;
                Location playerLoc = p.getLocation();
                if (args.length == 0) {
                    p.sendMessage("Usage: /meteor [firey/none] | 'Firey' fills the crater with fire, while 'none' gives you a crater without fire.");
                } else {
                    if (args[0] == null) {
                        p.sendMessage("Usage: /meteor [firey/none] | 'Firey' fills the crater with fire, while 'none' gives you a crater without fire.");
                    } else if (args[0].equalsIgnoreCase("firey")) {
                        MeteorEntity.firey = true;
                        net.minecraft.server.v1_7_R4.World world = ((CraftWorld) playerLoc
                                .getWorld()).getHandle();
                        MeteorEntity e = new MeteorEntity(world);
                        e.spawnIn(world);
                        e.setPosition(p.getLocation().getX(), p.getLocation()
                                .getY() + 10, p.getLocation().getZ());
                        int x = GenRandom.genNeg(20, -20);
                        int z = GenRandom.genNeg(20, -20);
                        e.setDirection(x, -7, z);
                        world.addEntity(e, SpawnReason.NATURAL);
                    } else if (args[0].equalsIgnoreCase("none")) {
                        MeteorEntity.firey = false;
                        net.minecraft.server.v1_7_R4.World world = ((CraftWorld) playerLoc
                                .getWorld()).getHandle();
                        MeteorEntity e = new MeteorEntity(world);
                        e.spawnIn(world);
                        e.setPosition(p.getLocation().getX(), p.getLocation()
                                .getY() + 10, p.getLocation().getZ());
                        int x = GenRandom.genNeg(20, -20);
                        int z = GenRandom.genNeg(20, -20);
                        e.setDirection(x, -7, z);
                        world.addEntity(e, SpawnReason.NATURAL);
                    } else {
                        p.sendMessage("Usage: /meteor [firey/none] | 'Firey' fills the crater with fire, while 'none' gives you a crater without fire.");
                    }
                }
            }
            }
        }
        return false;
    }
}
